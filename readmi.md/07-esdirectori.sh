#! /bin/bash
# @jhordin ASIX M01-ISO
# febrer 2022
# llistar directori
---------------------
ERR_ARGS=1
ERR_NODIR=2
#1) si num args no es correcte plegar 
if [ $# -ne 1 ]; then 
   echo "ERROR: num args incorrecte"
   echo "USAGE: $0 dir"
   exit $ERR_NODIR 
fi
#2) si no es un directori plegar 
if [ ! -d $1 ]; then 
   echo "ERROR: $1 no es un directori"
   echo "USAGE: $0 dir"
   exit $ERR_NODIR
fi 
#xixa
dir=$1
ls $dir
exit 0
