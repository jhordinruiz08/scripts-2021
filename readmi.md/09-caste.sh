#! /bin/bash
# @jhordin ASIX M01-ISO
# febrer 2022
# Exemples case 
------------------
# dl dt dc dj dv laborable
# ds dm festiu
case ["dl"|"dt"|"dc"|"dv" ])
	echo "$1 laborable";;
        ["ds"|"dm" ])
		echo "$1 és festiu"
exit 0
case $1 in
	[aeiou])
		echo "$1 es una vocal";;
	[bcdfgjklmnñpqrstvwxyz])
	        echo "$1 és una consonant";;
	*)
		echo "$1 és una altra cosa"
esac
exit 0
case $1 in
	"pere"|"pau"|"joan")
		echo "és un nen"
		;;
        "marta"|"anna"|"julia")
		echo "és una nena"
		;;
	*)
		echo "és indefinit";;
esac 
exit 0

		
