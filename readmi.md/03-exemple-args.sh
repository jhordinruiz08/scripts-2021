#! /bin/bash
# @jhordin ASIX M01-ISO
# febrer 2022
# exemple if:indica si és major d'edat
#     $ prog edat
#----------------------------------------
# 1) Validem arguments
 if [ $# -ne 1 ]
 then 
       echo "Error:numero d'arguments incorrecte"
       echo "Usage: $0 edat"
       exit 1
 fi
# 2) xixa
edat=$1
if [ $edat -gt 18 ]
then
	echo "edat $edat es major d'edat"
else 
	echo "edat $edat es menor d'edat"
fi 
exit 0
