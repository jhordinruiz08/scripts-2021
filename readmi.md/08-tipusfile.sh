#! /bin/bash
# @jhordin ASIX M01-ISO 
# febrer 2022
# nombar tipos de file 
---------------------------
ERR_ARGS=1
ERR_NOEXSIT=2
#1) si num args no es correcte plegar 
if [ $# -ne 1 ]; then 
   echo "ERROR: num args incorrecte"
   echo "USAGE: $0 dir"
   exit $ERR_ARGS
fi
# xixa
fit=$1
if ! [ -e $fit ]; then 
	echo "$fit no existeix"
	exit $ERR_NOEXIS
elif [ -f $fit ]; then
	echo "$fit ès un regular file"
elif [ -h $fit ]; then 
	echo "$fi ès un link"
elif [ -d $fit ]; then
	echo "$fi ès un directori"
else
	echo "$fi ès una alta cosa
fi
exit 0


