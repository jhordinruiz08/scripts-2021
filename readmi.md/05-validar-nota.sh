#! /bin/bash
# @jhordin ASIX M01-ISO
# febrer 2022
#validar nota: suspès, aprovat
#---------------------------------
ERR_ARGS=1
ERR_NOTA=2
#1) si num args no es correcte plegar
if [ $#  -ne 1 ]
then 
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi
#2) validar rang nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then 
	echo "Error nota $1 no valida"
	echo "nota pren valors de 0 a 10"
	echo "Usage: $0 nota"
	exit $ERR_NOTA
fi
#xixa
nota=$1
if [ $nota -lt 5 ]; then
	echo "la nota $nota ès un suspes"
elif [ $nota -lt 7 ]; then
	echo "la nota $nota ès un aprovat"
elif [ $nota -lt 9 ]; then
	echo "la nota $nota ès un notable"
else 
	echo "la nota $nota ès un Excel·lent"
fi 
exit 0      
