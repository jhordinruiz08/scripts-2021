#! /bin/bash
# @jhordin ASIX-M01
# Febrer 2022
# 
# Example de primer programa
# Normes:
#     shebang
#     capçalera:descripcio, data, autor
#-----------------------------------------
# es pot fer tot allo que es fa a la linea 
# dc comandes  
echo "hello world"
nom='pere poiu prat'
edat= 24
echo $nom $edat
echo -e "nom: $nom\n edat: $edat\n"
echo -e 'nom: $nom\n edat: $edat\n'
uaname -a
uptime
echo $SHLV
echo $SHELL
echo $((4*32))
echo $((edat*3))
read data1 data2 
echo -e "$data1 \n data2"
exit 0

